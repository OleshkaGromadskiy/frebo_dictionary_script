import 'text_formater.dart';

class ExcelLanguage {
  final String languageName;
  Map<String, String> properties = Map<String, String>();

  ExcelLanguage({this.languageName});

  @override
  String toString(){
    return 
    'import \'package:freezed_annotation/freezed_annotation.dart\';\n\n'
    'part \'${TextFormatter.toSnake(languageName)}.g.dart\';\n'
    'part \'${TextFormatter.toSnake(languageName)}.freezed.dart\';\n'
    '\n'
    '@freezed\n'
    'abstract class ${TextFormatter.toUpperCamel(languageName)} with _\$${TextFormatter.toUpperCamel(languageName)} {\n'
    '@JsonSerializable()\n'
    ' const factory ${TextFormatter.toUpperCamel(languageName)}({\n'
    '${_getJsonKeys()}\n'
    ' }) = _${TextFormatter.toUpperCamel(languageName)};\n'
    '\n'
    'factory ${TextFormatter.toUpperCamel(languageName)}.fromJson(Map json) => _\$${TextFormatter.toUpperCamel(languageName)}FromJson(json);\n'
    '}';
  }

  String _getJsonKeys(){
    return properties.entries.map((e) => '\t@JsonKey(name: \'${e.key}\') String ${TextFormatter.toCamel(e.key)}').join(',\n');
  }
}