class TextFormatter {
  TextFormatter._();

  static String toCamel(String string){
    return string.replaceAll(' ', '_').split('_').map((e) => e.toFirstUpper()).join().toFirstLower();
  }

  static String toUpperCamel(String string){
    return string.replaceAll(' ', '_').split('_').map((e) => e.toFirstUpper()).join() + "Language";
  }

  static String toSnake(String string){
    return string.split(' ').join('_').toLowerCase();
  }

}

extension ToCamel on String{
  String toFirstLower(){
    return "${this[0].toLowerCase()}${this.substring(1)}";
  }
  String toFirstUpper(){
    return "${this[0].toUpperCase()}${this.substring(1)}";
  }
}