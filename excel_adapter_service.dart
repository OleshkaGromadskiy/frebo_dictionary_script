import 'package:excel/excel.dart';
import 'excel_language.dart';
import 'dart:io';
import 'text_formater.dart';
import 'main.dart';

class ExcelAdapterService {
  List<List<dynamic>> rows;
  List<ExcelLanguage> languages;

  ExcelAdapterService(String filePath) {
    rows = Excel.decodeBytes(File(filePath).readAsBytesSync())
        .tables['Final']
        .rows;
    rows.removeAt(0);
  }

  void fillExcelLanguages() {
    languages = rows
        .map((element) => element[0])
        .toSet()
        .map((e) => ExcelLanguage(languageName: e))
        .toList()
          ..removeWhere((element) => element.languageName == null);

    languages.forEach((element) {
      print(element.languageName);
    });

    for (var item in rows) {
      languages
          .where((element) => element.languageName == item[0])
          .single
          .properties
          .addAll({item[1]: item[modelName == 'he' ? 2 : 3]});//1:2 - he, 1:3 - en

      languages.forEach((element) => element.properties
          .removeWhere((key, value) => key == null || value == null));
    }
  }

  void buildModels() {
    try {
      Directory('models').deleteSync(recursive: true);
      Directory('data').deleteSync(recursive: true);
    } catch (e) {}
    Directory('models').createSync(recursive: false);
      Directory('data').createSync(recursive: false);

    for (var lang in languages) {
      Directory('models/${TextFormatter.toSnake(lang.languageName)}')
          .create(recursive: false);
      File('models/${TextFormatter.toSnake(lang.languageName)}/${TextFormatter.toSnake(lang.languageName)}.dart')
          .create()
          .then((value) => value.writeAsString(lang.toString()));
    }
    Directory('models/language').createSync(recursive: false);
    File('models/language/language.dart').create().then((value) => value.writeAsString(_languagesToModel(languages)));
    File('data/${modelName}.dart').create().then((value) => value.writeAsString(_languagesToModelObject(languages)));
  }

  String _languagesToModel(List<ExcelLanguage> languages){
    return
    '${languages.map((e) => 'import \'package:frebo/dictionary/models/' + TextFormatter.toSnake(e.languageName) + '/' + TextFormatter.toSnake(e.languageName) + '.dart\';\n').join()}'
    'import \'package:freezed_annotation/freezed_annotation.dart\';\n\n'
    'part \'language.freezed.dart\';\n'
    'part \'language.g.dart\';\n\n'
    '@freezed\n'
    'abstract class Language with _\$Language {\n'
    '  @JsonSerializable()\n'
    '  const factory Language({\n'
    '    @required String name,\n'
    '${languages.map((e) => '\t\t@required ' + TextFormatter.toUpperCamel(e.languageName) + ' ' + TextFormatter.toCamel(e.languageName) + ',\n').join()}'
    '  }) = _Language;\n\n'
    '\tfactory Language.fromJson(Map json) => _\$LanguageFromJson(json);\n'
    '}';
  }

  String _languagesToModelObject(List<ExcelLanguage> languages){
    return
    'import \'package:frebo/dictionary/models/language/language.dart\';\n\n'
    '${languages.map((e) => 'import \'package:frebo/dictionary/models/' + TextFormatter.toSnake(e.languageName) + '/' + TextFormatter.toSnake(e.languageName) + '.dart\';\n').join()}'
    '\n\n'
    'Language $modelName = Language(\n'
    '\tname: \'$table\',\n'
    '\t${languages.map((e) => '\t' + TextFormatter.toCamel(e.languageName) + ': ' + TextFormatter.toUpperCamel(e.languageName) + '(\n' + e.properties.entries.map((item) => '\t\t' + TextFormatter.toCamel(item.key) + ': \'' + item.value.replaceAll('\n', '').replaceAll('\'', '\\\'') + '\',\n').join() +'\t),\n').join()}'
    ');';
  }
}
