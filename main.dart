import 'excel_adapter_service.dart';

const String modelName = 'en';
const String table = 'English';

void main()
{
  var service = ExcelAdapterService('dictionary.xlsx');

  service.fillExcelLanguages();
  service.buildModels();
}