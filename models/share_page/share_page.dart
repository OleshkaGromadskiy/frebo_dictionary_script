import 'package:freezed_annotation/freezed_annotation.dart';

part 'share_page.g.dart';
part 'share_page.freezed.dart';

@freezed
abstract class SharePageLanguage with _$SharePageLanguage {
@JsonSerializable()
 const factory SharePageLanguage({
	@JsonKey(name: 'Library_share_body') String libraryShareBody,
	@JsonKey(name: 'Library_share_subject') String libraryShareSubject,
	@JsonKey(name: 'Home_page_share_body') String homePageShareBody,
	@JsonKey(name: 'Home_page_share_subject') String homePageShareSubject,
	@JsonKey(name: 'book_page_share_body') String bookPageShareBody,
	@JsonKey(name: 'book_page_share_subject') String bookPageShareSubject
 }) = _SharePageLanguage;

factory SharePageLanguage.fromJson(Map json) => _$SharePageLanguageFromJson(json);
}