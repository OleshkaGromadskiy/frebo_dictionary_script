import 'package:freezed_annotation/freezed_annotation.dart';

part 'error.g.dart';
part 'error.freezed.dart';

@freezed
abstract class ErrorLanguage with _$ErrorLanguage {
@JsonSerializable()
 const factory ErrorLanguage({
	@JsonKey(name: 'Error_book_not_found') String errorBookNotFound,
	@JsonKey(name: 'Error_server') String errorServer,
	@JsonKey(name: 'Error_login_facebook_without_email') String errorLoginFacebookWithoutEmail,
	@JsonKey(name: 'Error_enough_books') String errorEnoughBooks,
	@JsonKey(name: 'Error_no_internet') String errorNoInternet
 }) = _ErrorLanguage;

factory ErrorLanguage.fromJson(Map json) => _$ErrorLanguageFromJson(json);
}