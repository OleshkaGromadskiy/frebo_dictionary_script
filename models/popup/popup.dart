import 'package:freezed_annotation/freezed_annotation.dart';

part 'popup.g.dart';
part 'popup.freezed.dart';

@freezed
abstract class PopupLanguage with _$PopupLanguage {
@JsonSerializable()
 const factory PopupLanguage({
	@JsonKey(name: 'Server_unavailable') String serverUnavailable,
	@JsonKey(name: 'Book_is_unavailable') String bookIsUnavailable,
	@JsonKey(name: 'Return_the_book') String returnTheBook,
	@JsonKey(name: 'Are_you_sure_cancel') String areYouSureCancel,
	@JsonKey(name: 'Are_you_sure_return') String areYouSureReturn,
	@JsonKey(name: 'Are_you_sure_taken') String areYouSureTaken,
	@JsonKey(name: 'Are_you_sure_leave') String areYouSureLeave,
	@JsonKey(name: 'Tap_one_more_time') String tapOneMoreTime,
	@JsonKey(name: 'Should_not_be_empty') String shouldNotBeEmpty,
	@JsonKey(name: 'Send') String send,
	@JsonKey(name: 'Your_message') String yourMessage,
	@JsonKey(name: 'Subject') String subject,
	@JsonKey(name: 'Contact_us') String contactUs,
	@JsonKey(name: 'Successfully_sent') String successfullySent,
	@JsonKey(name: 'AnyBook_team') String anyBookTeam,
	@JsonKey(name: 'ok') String ok,
	@JsonKey(name: 'Sorry_there_are_no_recommended_books') String sorryThereAreNoRecommendedBooks,
	@JsonKey(name: 'Popup_yes') String popupYes
 }) = _PopupLanguage;

factory PopupLanguage.fromJson(Map json) => _$PopupLanguageFromJson(json);
}