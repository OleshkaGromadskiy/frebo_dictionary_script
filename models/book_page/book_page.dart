import 'package:freezed_annotation/freezed_annotation.dart';

part 'book_page.g.dart';
part 'book_page.freezed.dart';

@freezed
abstract class BookPageLanguage with _$BookPageLanguage {
@JsonSerializable()
 const factory BookPageLanguage({
	@JsonKey(name: 'Book_page_summary_more') String bookPageSummaryMore,
	@JsonKey(name: 'Book_page_summary_less') String bookPageSummaryLess,
	@JsonKey(name: 'Book_page_friends_tab') String bookPageFriendsTab,
	@JsonKey(name: 'Book_page_reviews_tab_cancel') String bookPageReviewsTabCancel,
	@JsonKey(name: 'Book_page_friends_tab_waiting') String bookPageFriendsTabWaiting,
	@JsonKey(name: 'Book_page_friends_request_popup_text') String bookPageFriendsRequestPopupText,
	@JsonKey(name: 'Book_page_friends_request_popup_approveq') String bookPageFriendsRequestPopupApproveq,
	@JsonKey(name: 'Book_page_friends_request_no_copy_available_text') String bookPageFriendsRequestNoCopyAvailableText,
	@JsonKey(name: 'Book_page_friends_request_no_copy_available_button') String bookPageFriendsRequestNoCopyAvailableButton,
	@JsonKey(name: 'Book_page_reviews_tab') String bookPageReviewsTab,
	@JsonKey(name: 'Book_page_reviews_stars_title') String bookPageReviewsStarsTitle,
	@JsonKey(name: 'Book_page_reviews_stars_review_bar_text') String bookPageReviewsStarsReviewBarText,
	@JsonKey(name: 'Book_page_reviews_tab_edit_review_title') String bookPageReviewsTabEditReviewTitle,
	@JsonKey(name: 'Book_page_reviews_tab_edit_review_cancel') String bookPageReviewsTabEditReviewCancel,
	@JsonKey(name: 'Book_page_reviews_tab_edit_review_publish') String bookPageReviewsTabEditReviewPublish,
	@JsonKey(name: 'Book_page_reviews_tab_edit_review_Remove') String bookPageReviewsTabEditReviewRemove,
	@JsonKey(name: 'Book_page_book_request') String bookPageBookRequest,
	@JsonKey(name: 'Book_page_waiting') String bookPageWaiting,
	@JsonKey(name: 'Book_page_Remove') String bookPageRemove,
	@JsonKey(name: 'Book_page_address_enter') String bookPageAddressEnter,
	@JsonKey(name: 'Book_page_friend_in_your_library') String bookPageFriendInYourLibrary,
	@JsonKey(name: 'Book_page_reviews_title') String bookPageReviewsTitle,
	@JsonKey(name: 'Dialog_book_request') String dialogBookRequest,
	@JsonKey(name: 'Search_no_found') String searchNoFound,
	@JsonKey(name: 'Search_try_writing') String searchTryWriting
 }) = _BookPageLanguage;

factory BookPageLanguage.fromJson(Map json) => _$BookPageLanguageFromJson(json);
}