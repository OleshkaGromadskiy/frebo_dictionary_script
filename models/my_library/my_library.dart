import 'package:freezed_annotation/freezed_annotation.dart';

part 'my_library.g.dart';
part 'my_library.freezed.dart';

@freezed
abstract class MyLibraryLanguage with _$MyLibraryLanguage {
@JsonSerializable()
 const factory MyLibraryLanguage({
	@JsonKey(name: 'ok') String ok,
	@JsonKey(name: 'My_library_title') String myLibraryTitle,
	@JsonKey(name: 'My_library_my_genre') String myLibraryMyGenre,
	@JsonKey(name: 'My_library_add_books_to_library') String myLibraryAddBooksToLibrary,
	@JsonKey(name: 'Add_books_title') String addBooksTitle,
	@JsonKey(name: 'Add_books_scan_text1') String addBooksScanText1,
	@JsonKey(name: 'Add_books_search_link') String addBooksSearchLink,
	@JsonKey(name: 'Add_books_search_bar') String addBooksSearchBar,
	@JsonKey(name: 'Add_books_search_cancel') String addBooksSearchCancel,
	@JsonKey(name: 'Add_books_search_add_button') String addBooksSearchAddButton,
	@JsonKey(name: 'Add_books_scan_text2') String addBooksScanText2,
	@JsonKey(name: 'Add_books_done') String addBooksDone,
	@JsonKey(name: 'Library_edit') String libraryEdit,
	@JsonKey(name: 'My_library') String myLibrary,
	@JsonKey(name: 'Library_Without_Genre') String libraryWithoutGenre,
	@JsonKey(name: 'Library_no_books') String libraryNoBooks,
	@JsonKey(name: 'Library_loaned') String libraryLoaned
 }) = _MyLibraryLanguage;

factory MyLibraryLanguage.fromJson(Map json) => _$MyLibraryLanguageFromJson(json);
}