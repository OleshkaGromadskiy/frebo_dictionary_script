import 'package:freezed_annotation/freezed_annotation.dart';

part 'terms_page.g.dart';
part 'terms_page.freezed.dart';

@freezed
abstract class TermsPageLanguage with _$TermsPageLanguage {
@JsonSerializable()
 const factory TermsPageLanguage({
	@JsonKey(name: 'Terms_subtitle') String termsSubtitle,
	@JsonKey(name: 'Terms_body') String termsBody
 }) = _TermsPageLanguage;

factory TermsPageLanguage.fromJson(Map json) => _$TermsPageLanguageFromJson(json);
}