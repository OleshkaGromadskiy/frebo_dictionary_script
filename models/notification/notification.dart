import 'package:freezed_annotation/freezed_annotation.dart';

part 'notification.g.dart';
part 'notification.freezed.dart';

@freezed
abstract class NotificationLanguage with _$NotificationLanguage {
@JsonSerializable()
 const factory NotificationLanguage({
	@JsonKey(name: 'You_have_new_notification') String youHaveNewNotification,
	@JsonKey(name: 'Ok') String ok,
	@JsonKey(name: 'Show') String show,
	@JsonKey(name: 'Is_requested') String isRequested,
	@JsonKey(name: 'Is_located') String isLocated,
	@JsonKey(name: 'From_you') String fromYou,
	@JsonKey(name: 'Your_request_is_approved') String yourRequestIsApproved,
	@JsonKey(name: 'The') String the,
	@JsonKey(name: 'Book_was_returned') String bookWasReturned,
	@JsonKey(name: 'Was_returned') String wasReturned,
	@JsonKey(name: 'Book_was_taken') String bookWasTaken,
	@JsonKey(name: 'Was_taken') String wasTaken,
	@JsonKey(name: 'A_book_request_was_cancelled') String aBookRequestWasCancelled,
	@JsonKey(name: 'Wa_cancelled_by') String waCancelledBy
 }) = _NotificationLanguage;

factory NotificationLanguage.fromJson(Map json) => _$NotificationLanguageFromJson(json);
}