import 'package:freezed_annotation/freezed_annotation.dart';

part 'request_page.g.dart';
part 'request_page.freezed.dart';

@freezed
abstract class RequestPageLanguage with _$RequestPageLanguage {
@JsonSerializable()
 const factory RequestPageLanguage({
	@JsonKey(name: 'Request_page_title') String requestPageTitle,
	@JsonKey(name: 'Request_last_update') String requestLastUpdate,
	@JsonKey(name: 'Request_page_tabs_all') String requestPageTabsAll,
	@JsonKey(name: 'Request_page_tabs_my_requests') String requestPageTabsMyRequests,
	@JsonKey(name: 'Request_page_tabs_asked_me') String requestPageTabsAskedMe,
	@JsonKey(name: 'Request_page_status_waiting') String requestPageStatusWaiting,
	@JsonKey(name: 'Request_page_status_clock_days_left') String requestPageStatusClockDaysLeft,
	@JsonKey(name: 'Request_page_status_approved_title') String requestPageStatusApprovedTitle,
	@JsonKey(name: 'Request_page_status_approved_text') String requestPageStatusApprovedText,
	@JsonKey(name: 'Request_page_status_cancel') String requestPageStatusCancel,
	@JsonKey(name: 'Request_page_a_side_picked_up') String requestPageASidePickedUp,
	@JsonKey(name: 'Request_page_b_side_approve_button') String requestPageBSideApproveButton,
	@JsonKey(name: 'Request_page_popup_a_side_taken') String requestPagePopupASideTaken,
	@JsonKey(name: 'Request_page_b_side_direct_text1') String requestPageBSideDirectText1,
	@JsonKey(name: 'Request_page_b_side_open_text1') String requestPageBSideOpenText1,
	@JsonKey(name: 'Request_page_b_side_open_text2') String requestPageBSideOpenText2,
	@JsonKey(name: 'Request_page_a_side_direct_text1') String requestPageASideDirectText1,
	@JsonKey(name: 'Request_page_a_side_open_text1') String requestPageASideOpenText1,
	@JsonKey(name: 'Request_page_a_side_got_approved') String requestPageASideGotApproved,
	@JsonKey(name: 'Request_page_b_side_taken_button') String requestPageBSideTakenButton,
	@JsonKey(name: 'Request_page_b_side_Returned_button') String requestPageBSideReturnedButton,
	@JsonKey(name: 'Request_page_approve_form_title') String requestPageApproveFormTitle,
	@JsonKey(name: 'Request_page_approve_form_pickup location') String requestPageApproveFormPickupLocation,
	@JsonKey(name: 'Request_page_approve_form_pickup_data') String requestPageApproveFormPickupData,
	@JsonKey(name: 'Request_page_approve_form_pickup location_Option1') String requestPageApproveFormPickupLocationOption1,
	@JsonKey(name: 'Request_page_approve_form_pickup location_Option2') String requestPageApproveFormPickupLocationOption2,
	@JsonKey(name: 'Request_page_approve_form_pickup location_Option3') String requestPageApproveFormPickupLocationOption3,
	@JsonKey(name: 'Request_page_approve_form_when') String requestPageApproveFormWhen,
	@JsonKey(name: 'Request_page_approve_form_when_option1') String requestPageApproveFormWhenOption1,
	@JsonKey(name: 'Request_page_approve_form_when_option2') String requestPageApproveFormWhenOption2,
	@JsonKey(name: 'Request_page_approve_form_time') String requestPageApproveFormTime,
	@JsonKey(name: 'Request_page_approve_form_time_option1') String requestPageApproveFormTimeOption1,
	@JsonKey(name: 'Request_page_approve_form_time_option2') String requestPageApproveFormTimeOption2,
	@JsonKey(name: 'Request_page_approve_form_time_option3') String requestPageApproveFormTimeOption3,
	@JsonKey(name: 'Request_page_approve_form_approve_button') String requestPageApproveFormApproveButton,
	@JsonKey(name: 'Request_page_chat_bar') String requestPageChatBar,
	@JsonKey(name: 'Request_page_cancel_button') String requestPageCancelButton,
	@JsonKey(name: 'Chat_page_approve_at') String chatPageApproveAt,
	@JsonKey(name: 'Requiting_book_from_you') String requitingBookFromYou,
	@JsonKey(name: 'Chat_pickup_address') String chatPickupAddress,
	@JsonKey(name: 'Request_return_details') String requestReturnDetails,
	@JsonKey(name: 'Request_not_possible') String requestNotPossible,
	@JsonKey(name: 'Chat_page_you_requesting_book') String chatPageYouRequestingBook,
	@JsonKey(name: 'Chat_from') String chatFrom,
	@JsonKey(name: 'Request_you_approved_text') String requestYouApprovedText,
	@JsonKey(name: 'Request_picked') String requestPicked,
	@JsonKey(name: 'Request_type_book_returned') String requestTypeBookReturned,
	@JsonKey(name: 'Request') String request,
	@JsonKey(name: 'Request_no_requests') String requestNoRequests,
	@JsonKey(name: 'Request_before_time') String requestBeforeTime
 }) = _RequestPageLanguage;

factory RequestPageLanguage.fromJson(Map json) => _$RequestPageLanguageFromJson(json);
}