import 'package:freezed_annotation/freezed_annotation.dart';

part 'home_page.g.dart';
part 'home_page.freezed.dart';

@freezed
abstract class HomePageLanguage with _$HomePageLanguage {
@JsonSerializable()
 const factory HomePageLanguage({
	@JsonKey(name: 'HP_search_bar_text') String hPSearchBarText,
	@JsonKey(name: 'HP_Title_recomended') String hPTitleRecomended,
	@JsonKey(name: 'Search_cancel') String searchCancel
 }) = _HomePageLanguage;

factory HomePageLanguage.fromJson(Map json) => _$HomePageLanguageFromJson(json);
}