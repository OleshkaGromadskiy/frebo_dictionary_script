import 'package:freezed_annotation/freezed_annotation.dart';

part 'global.g.dart';
part 'global.freezed.dart';

@freezed
abstract class GlobalLanguage with _$GlobalLanguage {
@JsonSerializable()
 const factory GlobalLanguage({
	@JsonKey(name: 'Global_meter') String globalMeter,
	@JsonKey(name: 'Global_kilometer') String globalKilometer
 }) = _GlobalLanguage;

factory GlobalLanguage.fromJson(Map json) => _$GlobalLanguageFromJson(json);
}