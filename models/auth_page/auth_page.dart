import 'package:freezed_annotation/freezed_annotation.dart';

part 'auth_page.g.dart';
part 'auth_page.freezed.dart';

@freezed
abstract class AuthPageLanguage with _$AuthPageLanguage {
@JsonSerializable()
 const factory AuthPageLanguage({
	@JsonKey(name: 'Onboarding_1') String onboarding1,
	@JsonKey(name: 'Onboarding_2') String onboarding2,
	@JsonKey(name: 'Onboarding_3') String onboarding3,
	@JsonKey(name: 'Terms_of_use') String termsOfUse,
	@JsonKey(name: 'Terms_of_use_link') String termsOfUseLink,
	@JsonKey(name: 'Login_with_facebook') String loginWithFacebook,
	@JsonKey(name: 'Allow_us_to know_you_better_title') String allowUsToKnowYouBetterTitle,
	@JsonKey(name: 'Allow_us_to know_you_better_text') String allowUsToKnowYouBetterText,
	@JsonKey(name: 'Allow_us_to_continue') String allowUsToContinue,
	@JsonKey(name: 'What_is_your_home_address_title') String whatIsYourHomeAddressTitle,
	@JsonKey(name: 'What_is_your_home_address_text') String whatIsYourHomeAddressText,
	@JsonKey(name: 'Your_address') String yourAddress,
	@JsonKey(name: 'What_is_your_home_address_continue') String whatIsYourHomeAddressContinue,
	@JsonKey(name: 'What_is_your_home_address_later') String whatIsYourHomeAddressLater,
	@JsonKey(name: 'What_is_your_home_address_cancel') String whatIsYourHomeAddressCancel,
	@JsonKey(name: 'Tell_us_about_yourself_title') String tellUsAboutYourselfTitle,
	@JsonKey(name: 'Tell_us_about_yourself_man') String tellUsAboutYourselfMan,
	@JsonKey(name: 'Tell_us_about_yourself_woman') String tellUsAboutYourselfWoman,
	@JsonKey(name: 'Tell_us_about_yourself_not_disclose') String tellUsAboutYourselfNotDisclose,
	@JsonKey(name: 'Tell_us_about_yourself_birthdate_title') String tellUsAboutYourselfBirthdateTitle,
	@JsonKey(name: 'Tell_us_about_yourself_gender_day') String tellUsAboutYourselfGenderDay,
	@JsonKey(name: 'Tell_us_about_yourself_gender_month') String tellUsAboutYourselfGenderMonth,
	@JsonKey(name: 'Tell_us_about_yourself_gender_year') String tellUsAboutYourselfGenderYear,
	@JsonKey(name: 'Tell us about yourself_later') String tellUsAboutYourselfLater,
	@JsonKey(name: 'Tell us about yourself_continue') String tellUsAboutYourselfContinue,
	@JsonKey(name: 'Profiling_title') String profilingTitle,
	@JsonKey(name: 'Profiling_later') String profilingLater,
	@JsonKey(name: 'Profiling_continue') String profilingContinue,
	@JsonKey(name: 'Page_loader_title') String pageLoaderTitle,
	@JsonKey(name: 'Page_loader_text') String pageLoaderText,
	@JsonKey(name: 'Login_with_apple') String loginWithApple
 }) = _AuthPageLanguage;

factory AuthPageLanguage.fromJson(Map json) => _$AuthPageLanguageFromJson(json);
}