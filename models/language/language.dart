import 'package:frebo/dictionary/models/home_page/home_page.dart';
import 'package:frebo/dictionary/models/auth_page/auth_page.dart';
import 'package:frebo/dictionary/models/book_page/book_page.dart';
import 'package:frebo/dictionary/models/request_page/request_page.dart';
import 'package:frebo/dictionary/models/my_library/my_library.dart';
import 'package:frebo/dictionary/models/popup/popup.dart';
import 'package:frebo/dictionary/models/notification/notification.dart';
import 'package:frebo/dictionary/models/terms_page/terms_page.dart';
import 'package:frebo/dictionary/models/error/error.dart';
import 'package:frebo/dictionary/models/global/global.dart';
import 'package:frebo/dictionary/models/share_page/share_page.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'language.freezed.dart';
part 'language.g.dart';

@freezed
abstract class Language with _$Language {
  @JsonSerializable()
  const factory Language({
    @required String name,
		@required HomePageLanguage homePage,
		@required AuthPageLanguage authPage,
		@required BookPageLanguage bookPage,
		@required RequestPageLanguage requestPage,
		@required MyLibraryLanguage myLibrary,
		@required PopupLanguage popup,
		@required NotificationLanguage notification,
		@required TermsPageLanguage termsPage,
		@required ErrorLanguage error,
		@required GlobalLanguage global,
		@required SharePageLanguage sharePage,
  }) = _Language;

	factory Language.fromJson(Map json) => _$LanguageFromJson(json);
}